<?php

	class DiviBars_Model extends DiviBars {
		
		protected static $_show_errors = FALSE;
		
		public function __construct() {
			
			
		}
		
		
		public static function getDivibars( $type ) {
			
			global $wp_query;
			
			$posts = array();
			
			switch ( $type ) {
				
				case 'css_trigger':
				
					$args = array(
						'meta_key'   => 'dib_css_selector',
						'meta_value' => '',
						'meta_compare' => '!=',
						'post_type' => 'divi_bars',
						'posts_per_page' => -1,
						'post_status' => 'publish',
						'cache_results'  => false
					);
					$query = new WP_Query( $args );
					
					$posts = $query->get_posts();
					
					break;
					
				case 'enableurltrigger':
					
					$args = array(
						'meta_key'   => 'dib_enableurltrigger',
						'meta_value' => '1',
						'meta_compare' => '=',
						'post_type' => 'divi_bars',
						'posts_per_page' => -1,
						'post_status' => 'publish',
						'cache_results'  => false
					);
					$query = new WP_Query( $args );
					
					$posts = $query->get_posts();
					
					break;
					
				case 'customizeclosebtn':
					
					$args = array(
						'meta_key'   => 'post_do_customizeclosebtn',
						'meta_value' => '',
						'meta_compare' => '!=',
						'post_type' => 'divi_bars',
						'posts_per_page' => -1,
						'post_status' => 'publish',
						'cache_results'  => false
					);
					$query = new WP_Query( $args );
					
					$posts = $query->get_posts();
					
					break;
				
				case 'automatic_trigger':
			
					$args = array(
						'meta_key'   => 'divibars_automatictrigger',
						'meta_value' => '',
						'meta_compare' => '!=',
						'post_type' => 'divi_bars',
						'posts_per_page' => -1,
						'post_status' => 'publish',
						'cache_results'  => false
					);
					$query = new WP_Query( $args );
					
					$posts = $query->get_posts();
					
					break;
				
				case 1:
				
					
					break;
				
				default:
				
					return 'First parameter is required. For e.g.: "css_trigger", "enableurltrigger", "customizeclosebtn", "automatic_trigger"';
			}
			
			return $posts;
		}
		
	} // end DiviBars_Model