<?php
/*
Plugin Name: Divi Bars
Plugin URL: https://divilife.com/
Description: Create slide-in promo bars using the Divi Builder.
Version: 1.8.5
Author: Divi Life — Tim Strifler
Author URI: https://divilife.com
*/

// Make sure we don't expose any info if called directly or may someone integrates this plugin in a theme
if ( class_exists('DiviBars') || !defined('ABSPATH') || !function_exists( 'add_action' ) ) {
	
	return;
}

define( 'DIVI_BARS_VERSION', '1.8.5');
define( 'DIVI_BARS_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( 'DIVI_BARS_PLUGIN_NAME', trim( dirname( DIVI_BARS_PLUGIN_BASENAME ), '/' ) );
define( 'DIVI_BARS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'DIVI_BARS_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define( 'DIVI_SERVER_TIMEZONE', 'UTC');
define( 'DIVI_SCHEDULING_DATETIME_FORMAT', 'm\/d\/Y g:i A');

require_once( DIVI_BARS_PLUGIN_DIR . '/class.divi-bars.core.php' );

add_action( 'init', array( 'DiviBars', 'init' ) );
	
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
	
	require_once( DIVI_BARS_PLUGIN_DIR . '/class.divi-bars.admin.core.php' );
	add_action( 'init', array( 'DiviBars_Admin', 'init' ) );
}


// Load the API Key library if it is not already loaded
if ( ! class_exists( 'edd_divibars' ) ) {
	
	require_once( plugin_dir_path( __FILE__ ) . 'updater.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'updater-admin.php' );
}