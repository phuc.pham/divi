<?php
/**
* The base configuration for WordPress
*
* The wp-config.php creation script uses this file during the
* installation. You don't have to use the web site, you can
* copy this file to "wp-config.php" and fill in the values.
*
* This file contains the following configurations:
*
* * MySQL settings
* * Secret keys
* * Database table prefix
* * ABSPATH
*
* @link https://codex.wordpress.org/Editing_wp-config.php
*
* @package WordPress
*/

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rem');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Root@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY', 'UQ1n08g1FlRHiUdKVJXiWjBqI9gsXbcLzzalPcvx3Kc+JRHwx/aCZMqKWd//3TDr');
define('SECURE_AUTH_KEY', 'QcPlOPHT1DNuH7NUFu0uvGrn5MSrjb72fQq2H8ZxYD72B812wUCrZA1es6mcQvdF');
define('LOGGED_IN_KEY', 'o7ebVjJJDV4VwDJxOKXgbp75SderNj0oruaQVFX8l8EGY3PRfU7MVN2ANZJAyfxS');
define('NONCE_KEY', '1U9iIlfN1EFW1uHLNor5lp7pWiZiismReyxEzS4J0V//h+7amA5+mUbKb1edfZFp');
define('AUTH_SALT', 'ZvceR3QluJKUh4/qQxJUkNrfITCFD413K1W4Y4pbeEE1NsLVhO7YDsswA7qftuSY');
define('SECURE_AUTH_SALT', 'KfncUBrV3jd3GvpgxKpGomecPnW+qlEOr6qCEI5vqf8RIQ45fM/lgDqQia56dh8h');
define('LOGGED_IN_SALT', 'CCoP0/yXPBaBnoEg7FzFwdVsW/pMlPAa6ExXueFn2VjYNSnUByAa+xOlxaXeuOSk');
define('NONCE_SALT', 'OtQh//z0Wy33E/L9AFdPIr9E6sydHwLMq53cNm+DCqejpzjuhhFG+sOgPPrPOP4K');

/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each
* a unique prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = 'rem_';


define('WP_ALLOW_MULTISITE', true);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
