<?php

	class DiviBars_Helper {
		
		public function __construct() {
			
		}
		
		public static function get_all_wordpress_menus(){
			return get_terms( 'nav_menu', array( 'hide_empty' => true ) ); 
		}
		
		
		public static function prepareBars( $key = NULL )
		{
			try {
				
				if ( !$key ) {
					
					throw new InvalidArgumentException( 'DiviBars_Helper::prepareBars > Required var $key');
					
				}
				
				// it is an url with hash divibars?
				if ( strpos( $key, "#" ) !== false ) {
					
					$exploded_url = explode( "#", $key );
					
					if ( isset( $exploded_url[1] ) ) {
						
						$key = str_replace( 'divibars-', '', $exploded_url[1] );
					}
				}
				
				$pos = strpos( $key, 'divibars-' );
				if ( $pos !== false ) {
					
					$key = preg_replace( '/[^0-9.]/', '', $key );
				}
				else {
					
					return NULL;
				}
				
				if ( $key == '' ) {
					return NULL;
				}
				
				if ( !self::barIsPublished( $key ) ) {
					
					return NULL;
				}
				
				return $key;
				
			} catch (Exception $e) {
				
				DiviBars_Controller::log( $e );
				
				return NULL;
			}
		}
		
		
		private static function barIsPublished( $key ) {
			
			try {
			
				$post = get_post_status( $key );
			
			} catch (Exception $e) {
			
				DiviBars_Controller::log( $e );
				
				return FALSE;
			}
			
			if ( $post != 'publish' ) {
				
				return FALSE;
			}
			
			return TRUE;
		}
		
		
		// Checks multidimensional array like in_array function.
		public static function in_multiarray($elem, $array)
		{
			$top = sizeof($array) - 1;
			$bottom = 0;
			while($bottom <= $top)
			{
				if($array[$bottom] == $elem)
					return true;
				else
					if(is_array($array[$bottom]))
						if(in_multiarray($elem, ($array[$bottom])))
							return true;
					   
				$bottom++;
			}       
			return false;
		}
		
		
		// Fastest way to check if a string is JSON
		public static function isJson($string) {
		
			json_decode($string);
			
			return ( json_last_error() == JSON_ERROR_NONE );
		}
		
		
		public static function convertDateToUTC( $date = null, $timezone = DIVI_SERVER_TIMEZONE, $format = DIVI_SCHEDULING_DATETIME_FORMAT ) {
			
			if ( $date === null ) {
				
				return;
			}
			
			if ( !self::validateDate( $date, $format ) ) {
				
				return;
			}
			
			$wp_timezone = get_option('timezone_string');
			
			if ( $wp_timezone !== false ) {
				
				$timezone = $wp_timezone;
			}
			
			if ( empty( $timezone ) ) {
				
				$timezone = self::getWPTimeZone();
			}
			
			$date = new DateTime( $date, new DateTimeZone( $timezone ) );
			$date->setTimezone( new DateTimeZone( DIVI_SERVER_TIMEZONE ) );
			$str_server_now = $date->format( $format );
			
			return $str_server_now;
		}
		
		
		public static function convertDateToUserTimezone( $date = null, $format = DIVI_SCHEDULING_DATETIME_FORMAT ) {
			
			if ( $date === null ) {
				
				return;
			}
			
			if ( !self::validateDate( $date, $format ) ) {
				
				return;
			}
			
			$timezone = DIVI_SERVER_TIMEZONE;
			
			$wp_timezone = get_option('timezone_string');
			
			if ( $wp_timezone !== false ) {
				
				$timezone = $wp_timezone;
			}
			
			if ( empty( $timezone ) ) {
				
				$timezone = self::getWPTimeZone();
			}
			
			$date = new DateTime( $date, new DateTimeZone( DIVI_SERVER_TIMEZONE ) );
			$date->setTimezone( new DateTimeZone( $timezone ) );
			$str_server_now = $date->format( $format );
			
			return $str_server_now;
		}
		
		
		public static function getWPTimeZone() {
			
			$offset  = get_option( 'gmt_offset' );
			$hours   = (int) $offset;
			$minutes = abs( ( $offset - (int) $offset ) * 60 );
			$offset  = sprintf( '%+03d:%02d', $hours, $minutes );
			
			// Calculate seconds from offset
			list($hours, $minutes) = explode(':', $offset);
			
			$seconds = $hours * 60 * 60 + $minutes * 60;
			
			// Get timezone name from seconds
			$timezone = timezone_name_from_abbr('', $seconds, 1);
			
			// Workaround for bug #44780
			if ( $timezone === false ) $timezone = timezone_name_from_abbr('', $seconds, 0);
			
			return $timezone;
		}
		
		
		public static function validateDate( $dateStr, $format ) {
			
			$date = DateTime::createFromFormat($format, $dateStr);
			
			return $date && ($date->format($format) === $dateStr);
		}
		
		
		public static function getDiviStylesManager() {
			
			if ( wp_doing_ajax() || wp_doing_cron() || ( is_admin() && ! is_customize_preview() ) ) {
				return;
			}

			/** @see ET_Core_SupportCenter::toggle_safe_mode */
			if ( et_core_is_safe_mode_active() ) {
				return;
			}

			$post_id     = et_core_page_resource_get_the_ID();
			$is_preview  = is_preview() || isset( $_GET['et_pb_preview_nonce'] ) || is_customize_preview();
			$is_singular = et_core_page_resource_is_singular();

			$disabled_global = 'off' === et_get_option( 'et_pb_static_css_file', 'on' );
			$disabled_post   = $disabled_global || ( $is_singular && 'off' === get_post_meta( $post_id, '_et_pb_static_css_file', true ) );

			$forced_inline     = $is_preview || $disabled_global || $disabled_post || post_password_required();
			$builder_in_footer = 'on' === et_get_option( 'et_pb_css_in_footer', 'off' );

			$unified_styles = $is_singular && ! $forced_inline && ! $builder_in_footer && et_core_is_builder_used_on_current_request();
			$resource_owner = $unified_styles ? 'core' : 'divi';
			$resource_slug  = $unified_styles ? 'unified' : 'customizer';
			$resource_slug .= $unified_styles && et_builder_post_is_of_custom_post_type( $post_id ) && et_pb_is_pagebuilder_used( $post_id ) ? '-cpt' : '';
			$css            = 'et_builder_maybe_wrap_css_selector';

			if ( $is_preview ) {
				// Don't let previews cause existing saved static css files to be modified.
				$resource_slug .= '-preview';
			}

			if ( function_exists( 'et_fb_is_enabled' ) && et_fb_is_enabled() ) {
				$resource_slug .= '-vb';
			}

			if ( ! $unified_styles ) {
				$post_id = 'global';
			}

			$styles_manager = et_core_page_resource_get( $resource_owner, $resource_slug, $post_id );
			
			return $styles_manager;
		}
	}
	
	